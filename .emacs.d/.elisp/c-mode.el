(defun my-c-mode-common-init ()
  (setq indent-tabs-mode t)
  (setq c-basic-offset 4)
)

(add-hook 'c-mode-hook 'my-c-mode-common-init)
(add-hook 'c++-mode-hook 'my-c-mode-common-init)

			      
