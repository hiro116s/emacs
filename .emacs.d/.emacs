;
;  ~/.emacs
;

;;; path
(setq load-path (cons "/usr/local/share/emacs/site-lisp/mew/" load-path))
(setq load-path (cons "~/.elisp/" load-path))

;;; key bind
(global-set-key "\C-o" 'toggle-input-method)
(global-set-key "\M-?" 'help-for-help)
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-h" (quote delete-backward-char))

;;; coloring parenthesis
(show-paren-mode 1)

(setq show-paren-style 'mixed)
(set-face-background 'show-paren-match-face "gray")
(set-face-foreground 'show-paren-match-face "red")

;; Don't display the opening message
(setq inhibit-startup-message t)

;;; environment settijjng
;(setq next-line-add-newlines nil)
;(setq-default fill-column 60)
;(cond (window-system (setq hilit-mode-enable-list  '(not text-mode)
;                           hilit-background-mode   'light
;                           hilit-inhibit-hooks     nil
;                           hilit-inhibit-rebinding nil)
;                     (global-unset-key "\C-z")
;                     (if (x-display-color-p) (require 'hilit19)))
;      (t (menu-bar-mode nil)))

;;; K&R 2nd Edition like c-mode
(require 'cc-mode)
; (require 'un-define)
; (require 'jisx0213)

;;; Mew
(autoload 'mew "mew" nil t)
(autoload 'mew-send "mew" nil t)
(setq mew-mail-domain "am.ics.keio.ac.jp")
(setq mew-smtp-server "mail.am.ics.keio.ac.jp")
(setq mew-mailbox-type `mbox)
(setq mew-mbox-command "incm")
(setq mew-mbox-command-arg (concat "-u -d " (expand-file-name "~/.Maildir")))
(setq mew-auto-flush-queue nil)
(setq mew-ask-subject t)
(setq mew-fcc "+sent")
(setq mew-mail-domain-list '("am.ics.keio.ac.jp"))
(setq mew-reply-all-alist '((("Followup-To:" "poster") ("To:" "From:"))
                            ("Followup-To:" ("Newsgroups:" "Followup-To:"))
                            ("Newsgroups:" ("Newsgruops:" "Newsgroups:"))
                            ("Reply-To:" ("To:" "Reply-To:"))
                            (t ("To:" "From:"))))
(setq mew-reply-sender-alist '(("Reply-To:" ("To:" "Reply-To:"))
                               (t ("To:" "From:"))))
;(setq mew-summary-show-direction 'stop)
;(setq mew-cite-fields nil)
;(setq mew-signature-insert-last t)
(add-hook 'mew-env-hook 
	  '(lambda () 
	     (setq mew-icon-directory
              "/usr/local/share/emacs/site-lisp/mew/etc")))

;;; Migemo
;;(load "migemo.el")

;;; Input Method ;;;

;; There is no "anthy" file, so it is comment out
;;(setq emacs-ime (getenv "anthy"))
;;(setq default-input-method "japanese-anthy")
;;(load-library "anthy")

(custom-set-variables
  ;; custom-set-variables was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 '(auto-compression-mode t nil (jka-compr))
 '(case-fold-search t)
 '(current-language-environment "Japanese")
 '(default-input-method "japanese-anthy")
 '(global-font-lock-mode t nil (font-lock))
 '(initial-frame-alist (quote ((menu-bar-lines . 1) (tool-bar-lines . 1) (height . 100) (eidth . 80))))
 '(show-paren-mode t nil (paren)))
(custom-set-faces
  ;; custom-set-faces was added by Custom.
  ;; If you edit it by hand, you could mess it up, so be careful.
  ;; Your init file should contain only one such instance.
  ;; If there is more than one, they won't work right.
 )

;;; YaTeX-mode -------------------------------------------------------------
(setq auto-mode-alist  (cons (cons "\\.tex$" 'yatex-mode) auto-mode-alist))
(autoload 'yatex-mode "yatex" "Yet Another LaTeX mode" t)
(setq load-path (cons "~/.elisp" load-path))
(setq tex-command "platex")
(setq dvi2-command "c:/tex/dviout/dviout")
(setq dviprint-command-format "dvipdfmx %s ")
;; -------------------------------------------------------------------------

;;;Verilog;;;------------------------------------------------------------
(load-file "~/.elisp/verilog-mode.el")

;; Load verilog mode only when needed
(autoload 'verilog-mode "verilog-mode" "Verilog mode" t )
;; Any files that end in .v should be in verilog mode
(setq auto-mode-alist (cons  '("\\.v\\'" . verilog-mode) auto-mode-alist))
(setq auto-mode-alist (cons  '("\\.vh\\'" . verilog-mode) auto-mode-alist))
(setq auto-mode-alist (cons  '("\\.test\\'" . verilog-mode) auto-mode-alist))
(setq auto-mode-alist (cons  '("\\.req\\'" . verilog-mode) auto-mode-alist))
;; Any files in verilog mode should have their keywords colorized
   (add-hook 'verilog-mode-hook ' (lambda () (font-lock-mode 1)))
;; ------------------------------------------------------------------------


(eval-after-load "ispell" '(add-to-list 'ispell-skip-region-alist '("[^\000-\377]+")))

;; まず、install-elisp のコマンドを使える様にします。
(require 'install-elisp)
;; 次に、Elisp ファイルをインストールする場所を指定します。
(setq install-elisp-repository-directory "~/.elisp/")

;; auto complete mode setting
(require 'auto-complete)
(global-auto-complete-mode t)

;; color theme setting
(require 'color-theme)
(color-theme-initialize)
(color-theme-clarity)

;; load setting file of C or C++.
(load-file "~/.elisp/c-mode.el")

(set-face-attribute 'default nil
		    :family "fixed") ;; font

;; default setting of char encode
;(set-language-environment 'utf-8)
(set-default-coding-systems 'utf-8)
(set-language-environment "Japanese")

(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)