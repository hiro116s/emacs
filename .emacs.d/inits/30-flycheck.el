;; flycheck
(require 'flycheck)
(flycheck-define-checker c/c++
  "A C/C++ checker using g++."
  :command ("g++" "-Wall" "-Wextra" source)
  :error-patterns  ((error line-start
                           (file-name) ":" line ":" column ":" " エラー: " (message)
                           line-end)
                    (warning line-start
			     (file-name) ":" line ":" column ":" " 警告: " (message)
			     line-end))
  :modes (c-mode c++-mode))
(add-hook 'c-mode-common-hook 'flycheck-mode)

(add-hook 'c++-mode-hook
	  (lambda () (setq flycheck-clang-include-path
			   (list (expand-file-name "~/trax/hlabdc/include/")))))

