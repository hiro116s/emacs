;; auto-complete-clang 設定
(require 'cl)
(require 'auto-complete-config)
(require 'auto-complete-clang-async)
(ac-config-default)

;;補完キー指定
(ac-set-trigger-key "TAB")
;;ヘルプ画面が出るまでの時間（秒）
(setq ac-clang-quick-help-delay 2)
(setq ac-clang-use-fuzzy t)

(add-hook 'c++-mode-hook
          '(lambda()
             (setq ac-clang-complete-executable "~/bin/clang-complete")
             (setq ac-sources '(ac-source-clang-async))
             (ac-clang-launch-completion-process)))

(defun recursive-load-dir-settings (currentfile)
  (let ((lds-dir (locate-dominating-file currentfile ".settings.el")))
  (when lds-dir
      (progn
        (load-file (concat lds-dir ".settings.el"))
        (recursive-load-dir-settings
         (file-truename(concat lds-dir "..")))))))

(defun load-dir-settings()
  (interactive)
  (when buffer-file-name
    (recursive-load-dir-settings buffer-file-name)))

(add-hook 'find-file-hook 'load-dir-settings)
