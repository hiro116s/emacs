;;; key bind
(global-set-key "\C-o" 'toggle-input-method)
(global-set-key "\M-?" 'help-for-help)
(global-set-key "\M-g" 'goto-line)
(global-set-key "\C-h" (quote delete-backward-char))

(global-set-key (kbd "C-c <left>")  'windmove-left)
(global-set-key (kbd "C-c <right>") 'windmove-right)
(global-set-key (kbd "C-c <up>")    'windmove-up)
(global-set-key (kbd "C-c <down>")  'windmove-down)


