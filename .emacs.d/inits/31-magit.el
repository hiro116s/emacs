(require 'magit-find-file) ;; if not using the ELPA package
(global-set-key (kbd "C-c p") 'magit-find-file-completing-read)
